def ftoc(degrees_in_farenheit)
  ((degrees_in_farenheit - 32) / (9.0 / 5)).round
end

def ctof(degrees_in_celsius)
  degrees_in_celsius / (5.0 / 9) + 32
end
