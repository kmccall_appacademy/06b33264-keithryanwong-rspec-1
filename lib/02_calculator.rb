def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  final_sum = 0
  arr.each { |num| final_sum += num }
  final_sum
end

def multiply(num1, num2, *numbers)
  product = num1 * num2
  numbers.each { |num| product *= num}
  product
end

def power(num1, power)
  return 1 if power == 0
  i = 1
  product = num1
  while i < power do
    product *= num1
    i += 1
  end
  product
end

def factorial(num)
  return 1 if num == 0
  product = 1
  (1..num).to_a.each { |integer| product *= integer }
  product
end
