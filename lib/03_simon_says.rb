def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, num=2)
  final_str = str.dup
  i = 1
  while i < num do
    final_str << " #{str}"
    i += 1
  end
  final_str
end

def start_of_word(word, num)
  word[0...num]
end

def first_word(str)
  str.split(' ')[0]
end

def titleize(str)
  cap_words(str.split(' ')).join(' ')
end

def cap_words(arr)
  conpreptions = [:over, :a, :an, :the, :and]
  arr.map.with_index do |word, index|
    (conpreptions.index(word.to_sym) && index != 0) ?
    word : word.capitalize
  end
end
