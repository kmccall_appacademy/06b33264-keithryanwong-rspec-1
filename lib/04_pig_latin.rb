def translate(str)
  translate_words(str.split(' ')).join(' ')
end

def translate_words(words)
  words.map do |word|
    translated_word = ''
    if word =~ /\?|\!|\,|\.|\;|\:|\'|\"/
      translated_word = translate_word(word[0...-1].downcase) + word[-1]
    else
      translated_word = translate_word(word.downcase)
    end
    translated_word.capitalize! if is_capitalized?(word)
    translated_word
  end
end

def translate_word(word)
  return word + 'ay' if is_vowel?(word[0])
  index = word =~ /a|e|i|o|u/
  if word[index] == 'u' && word[index - 1] == 'q'
    word[(index + 1)..-1].concat(word[0..index]).concat('ay')
  else
    word[index..-1].concat(word[0...index]).concat('ay')
  end
end

def is_vowel?(letter)
  vowels = [:a, :e, :i, :o, :u]
  vowels.include?(letter.to_sym)
end

def is_capitalized?(word)
  word[0] < 'a'
end
